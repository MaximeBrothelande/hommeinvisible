export const ADD_GROUP = 'ADD_GROUP'

export const addGroup = payload => ({
  type: ADD_GROUP,
  payload
})
