const themeLight = {
  primary: 'white',
  secondary: 'lightgray',
  tertiary: '#F0F0F0',
  text: 'black',
  formIn: '#483270',
  formOut: 'lightgray'
}

const themeDark = {
  primary: 'black',
  secondary: '#181818',
  tertiary: '#202020',
  text: 'white',
  formIn: '#483270',
  formOut: '#181818'
}

export { themeDark, themeLight }
